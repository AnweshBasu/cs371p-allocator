// ----------------------------------------
// projects/c++/allocator/TestAllocator.c++
// Copyright (C) 2018
// Glenn P. Downing
// ----------------------------------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#include <algorithm> // count
#include <memory>    // allocator

#include "gtest/gtest.h"

#include "Allocator.h"

// --------------
// TestAllocator1
// --------------

template <typename A>
struct TestAllocator1 : testing::Test {
    // --------
    // typedefs
    // --------

    typedef          A             allocator_type;
    typedef typename A::value_type value_type;
    typedef typename A::size_type  size_type;
    typedef typename A::pointer    pointer;
};

typedef testing::Types<
std::allocator<double>,
    std::allocator<double>,
    my_allocator<double, 1000>,
    my_allocator<double, 100>>
    my_types_1;

TYPED_TEST_CASE(TestAllocator1, my_types_1);

TYPED_TEST(TestAllocator1, test_1) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    const size_type  s = 1;
    const value_type v = 2;
    pointer p = x.allocate(1);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(v, *p);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(TestAllocator1, test_10) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(s, std::count(b, e, v));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

// --------------
// TestAllocator2
// --------------

// TEST(TestAllocator2, const_index) {
//     const my_allocator<int, 100> x;
//     ASSERT_EQ(x[0], 0);}

// TEST(TestAllocator2, index) {
//     my_allocator<int, 100> x;
//     ASSERT_EQ(x[0], 0);}

TEST(TestAllocator2, test1) {
    my_allocator<double, 100> x;
    x.allocate(5);
    ASSERT_EQ(x[0], -40);
    x.deallocate(x.getNthBusyBlock(-1), 0);
}

TEST(TestAllocator2, test2) {
    my_allocator<double, 1000> x;
    x.deallocate(x.allocate(5), 0);
    ASSERT_EQ(x[0], 992);
}

TEST(TestAllocator2, test3) {
    my_allocator<double, 1000> x;
    x.allocate(5);
    double * p = x.getNthBusyBlock(-1);
    char* c = reinterpret_cast<char*>(p);
    ASSERT_EQ(*reinterpret_cast<int*>(c), -40);
    x.deallocate(x.getNthBusyBlock(-1), 0);
}

// Coalesce on both sides.
TEST(TestAllocator2, test4) {
    my_allocator<double, 1000> x;
    double * p1 = x.allocate(5);
    double * p2 = x.allocate(4);
    x.deallocate(p1, 0);
    x.deallocate(p2, 0);
    ASSERT_EQ(x[0], 992);
}

TEST(TestAllocator2, test5) {
    my_allocator<double, 1000> x;
    double * p1 = x.allocate(5);
    double * p2 = x.allocate(4);
    x.allocate(3);
    x.deallocate(p1, 0);
    x.deallocate(x.getNthBusyBlock(-2), 0);
    x.deallocate(p2, 0);
    ASSERT_EQ(x[0], 992);
}

TEST(TestAllocator2, test6) {
    my_allocator<double, 100> x;
    x.allocate(3);
    x.allocate(2);
    x.allocate(1);
    x.deallocate(x.getNthBusyBlock(-1), 0);
    x.deallocate(x.getNthBusyBlock(-1), 0);
    ASSERT_EQ(x[0], 48);
    x.deallocate(x.getNthBusyBlock(-1), 0);
}

TEST(TestAllocator2, test7) {
    my_allocator<double, 1000> x;
    x.allocate(3);
    x.allocate(2);
    x.allocate(1);
    x.deallocate(x.getNthBusyBlock(-1), 0);
    x.deallocate(x.getNthBusyBlock(-1), 0);
    ASSERT_EQ(x[0], 48);
    x.deallocate(x.getNthBusyBlock(-1), 0);
}

TEST(TestAllocator2, test8) {
    my_allocator<double, 100> x;
    x.allocate(10);
    ASSERT_EQ(x[0], -92);
}

TEST(TestAllocator2, test9)  {
    my_allocator<double, 1000> x;
    ASSERT_EQ(x.get((char&)x[0]), 992);
}

TEST(TestAllocator2, test10) {
    my_allocator<double, 1000> x;
    x.set((char&)x[0], 991);
    ASSERT_EQ(x[0], 991);
}


// --------------
// TestAllocator3
// --------------

template <typename A>
struct TestAllocator3 : testing::Test {
    // --------
    // typedefs
    // --------

    typedef          A             allocator_type;
    typedef typename A::value_type value_type;
    typedef typename A::size_type  size_type;
    typedef typename A::pointer    pointer;
};

typedef testing::Types<
my_allocator<double,    1000>,
             my_allocator<double, 100>>
             my_types_2;

TYPED_TEST_CASE(TestAllocator3, my_types_2);

TYPED_TEST(TestAllocator3, test_1) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type   x;
    const size_type  s = 1;
    const value_type v = 2;
    const pointer    p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(v, *p);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(TestAllocator3, test_10) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(s, std::count(b, e, v));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}
