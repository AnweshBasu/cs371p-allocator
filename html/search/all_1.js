var searchData=
[
  ['checkcoalesce',['checkCoalesce',['../classmy__allocator.html#abbb2b36e54ed8a930f83441bd6687703',1,'my_allocator::checkCoalesce(char *index)'],['../classmy__allocator.html#abbb2b36e54ed8a930f83441bd6687703',1,'my_allocator::checkCoalesce(char *index)']]],
  ['coalesce',['coalesce',['../classmy__allocator.html#affcd842b5e61a53ab5d34653973cc6cf',1,'my_allocator::coalesce(char *leftIndex, char *rightIndex)'],['../classmy__allocator.html#affcd842b5e61a53ab5d34653973cc6cf',1,'my_allocator::coalesce(char *leftIndex, char *rightIndex)']]],
  ['const_5fpointer',['const_pointer',['../classmy__allocator.html#a9e881ea9cb2e89a5848b18a4b4de2391',1,'my_allocator::const_pointer()'],['../classmy__allocator.html#a9e881ea9cb2e89a5848b18a4b4de2391',1,'my_allocator::const_pointer()']]],
  ['const_5freference',['const_reference',['../classmy__allocator.html#a256c18d997824aa93f51f5aaf455083c',1,'my_allocator::const_reference()'],['../classmy__allocator.html#a256c18d997824aa93f51f5aaf455083c',1,'my_allocator::const_reference()']]],
  ['construct',['construct',['../classmy__allocator.html#ac682ce1ad46490b5e08f7a2a01b6cf2a',1,'my_allocator::construct(pointer p, const_reference v)'],['../classmy__allocator.html#ac682ce1ad46490b5e08f7a2a01b6cf2a',1,'my_allocator::construct(pointer p, const_reference v)']]]
];
